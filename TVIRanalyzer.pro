TARGET = TVIRanalyzer
TEMPLATE = app

# Config
QT        += gui
QT        += core
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
INCLUDEPATH += include

win32 {
    INCLUDEPATH += "F:\opencv\opencv\build\include" \

    CONFIG(debug,debug|release) {
        LIBS += -L"F:\opencv\opencv\build\x64\vc12\lib" \
            -lopencv_core2413d \
            -lopencv_highgui2413d \
            -lopencv_imgproc2413d \
            -lopencv_features2d2413d \
            -lopencv_gpu2413d \
            -lopencv_calib3d2413d \
            -lopencv_video2413d
    }

    CONFIG(release,debug|release) {
        DEFINES += QT_NO_WARNING_OUTPUT QT_NO_DEBUG_OUTPUT
        LIBS += -L"F:\opencv\opencv\build\x64\vc12\lib" \
            -lopencv_core2413 \
            -lopencv_highgui2413 \
            -lopencv_imgproc2413 \
            -lopencv_features2d2413 \
            -lopencv_gpu2413 \
            -lopencv_calib3d2413 \
            -lopencv_video2413
    }
}
# Headers
HEADERS += include/mainwindow.h
HEADERS += include/imageAnalysis.h

# Sources
SOURCES += src/mainwindow.cpp
SOURCES += src/main.cpp
SOURCES += src/imageAnalysis.cpp


# Forms
FORMS   += ui/mainwindow.ui



