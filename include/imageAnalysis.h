#ifndef IMAGEANALYSIS_H
#define IMAGEANALYSIS_H

#include <QElapsedTimer>
#include <QString>
#include <QStringList>
#include <QPair>
#include <opencv2/opencv.hpp>

using namespace cv;

struct AnalysisReport {
    Mat irImage;
    Mat tvImage;
    QString reportText = "";
};

class ImageAnalysis
{
public:
    ImageAnalysis();
    AnalysisReport analyze(int index);
    QStringList getAnalyzeMethodList();

    void setImageTV(const cv::Mat &value);
    void setImageIR(const cv::Mat &value);
    QString getElapsedTime();

private:
    AnalysisReport report;
    cv::Mat alphaBlending(cv::Mat &inputOne, cv::Mat &inputTwo);
    cv::Mat equalizeHistogram(cv::Mat &input);
    cv::Mat calculateHistogram(cv::Mat &input);
    cv::Mat findContoursInImage(cv::Mat &input);
    cv::Mat calculateNegative(cv::Mat &input);
    cv::Mat calculateNegativeWithThreshold(cv::Mat &input);
    cv::Mat calculateNegativeWithAutoThreshold(cv::Mat &inputForProc, cv::Mat inputForHist);
    cv::Mat calculateNegativeWithThresholdSharpened(cv::Mat &input);
    cv::Mat calculateNegativeSubstraction(cv::Mat &input);
    cv::Mat adaptiveMeanThresholding(cv::Mat &input, int alpha, int beta);
    cv::Mat adaptiveGaussThresholding(cv::Mat &input);
    cv::Mat openMorphologicaly(cv::Mat &input);
    cv::Mat highThreshold(cv::Mat &input);
    cv::Mat extractValidInformation(cv::Mat &input, int alpha, int beta);
    cv::Mat gaussianSubstraction(cv::Mat &input);
    cv::Mat sobelOperator(cv::Mat &input);
    cv::Mat laplaceOperator(cv::Mat &input);
    cv::Mat cannyEdges(cv::Mat &input);
    cv::Mat testingAnalyze(cv::Mat &input);
    QString calculateIndicators(cv::Mat &input, QString imageType);

    cv::Mat convertToGray(cv::Mat &input);
    cv::Mat imageTV;
    cv::Mat imageIR;
    QElapsedTimer elapsedTimer;
};

#endif // IMAGEANALYSIS_H
