#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QKeyEvent>
#include <QLabel>
#include <QStringListModel>
#include <opencv2/opencv.hpp>
#include <imageAnalysis.h>

using namespace std;
using namespace cv;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    //==============================================================
    //======================= PUBLIC METHODS =======================
    //==============================================================

    /** \brief Constructor */
    explicit MainWindow( int argc, char** argv, QWidget *parent = 0 );

    /** \brief Destructor */
    ~MainWindow();

private slots:
    void on_openTVImageButton_clicked();

    void on_openIRImageButton_clicked();

    void on_analyzeButton_clicked();

private:
    QString pathTV;
    QString pathIR;
    ImageAnalysis analyzer;

    /** \brief User interface object */
    Ui::MainWindow *ui;

    void displayImageRGB(cv::Mat &mat, QLabel *label);
    void displayImageGray(cv::Mat &mat, QLabel *label);
    Size getResizeSize(Size imageSize, Size frameSize);
};

#endif // MAINWINDOW_H
