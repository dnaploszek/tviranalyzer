﻿#include <imageAnalysis.h>

ImageAnalysis::ImageAnalysis()
{

}

AnalysisReport ImageAnalysis::analyze(int index)
{
    elapsedTimer.start();
    report.irImage = Mat();
    report.tvImage = Mat();
    report.reportText = "";
    switch(index) {
    case 0:
        report.tvImage = calculateHistogram(imageTV);
        report.irImage = calculateHistogram(imageIR);
        break;
    case 1:
        report.tvImage = equalizeHistogram(imageTV);
        report.irImage = equalizeHistogram(imageIR);
        break;
    case 2:
        report.tvImage = calculateNegative(imageTV);
        report.irImage = calculateNegative(imageIR);
        break;
    case 3:
        report.tvImage = calculateNegativeWithThreshold(imageTV);
        report.irImage = calculateNegativeWithThreshold(imageIR);
        break;
    case 4:
        report.tvImage = calculateNegativeWithAutoThreshold(imageTV, imageIR);
        report.irImage = calculateNegativeWithAutoThreshold(imageIR, imageTV);
        break;
    case 5:
        report.tvImage = calculateNegativeWithThresholdSharpened(imageTV);
        report.irImage = calculateNegativeWithThresholdSharpened(imageIR);
        break;
    case 6:
        report.tvImage = calculateNegativeSubstraction(imageTV);
        report.irImage = calculateNegativeSubstraction(imageIR);
        break;
    case 7:
        report.tvImage = equalizeHistogram(imageTV);
        report.irImage = equalizeHistogram(imageIR);
        report.tvImage = calculateNegative(report.tvImage);
        report.irImage = calculateNegative(report.irImage);
        break;
    case 8:
        report.tvImage = adaptiveMeanThresholding(imageTV, 11, 5);
        report.irImage = adaptiveMeanThresholding(imageIR, 11, 5);
        break;
    case 9:
        report.tvImage = adaptiveGaussThresholding(imageTV);
        report.irImage = adaptiveGaussThresholding(imageIR);
        break;
    case 10:
        report.irImage = calculateNegative(imageIR);
        report.tvImage = adaptiveMeanThresholding(imageTV, 11, 5);
        report.irImage = adaptiveMeanThresholding(report.irImage, 11, 5);
        break;
    case 11:
        report.tvImage = highThreshold(imageTV);
        report.irImage = highThreshold(imageIR);
        break;
    case 12:
        report.tvImage = gaussianSubstraction(imageTV);
        report.irImage = gaussianSubstraction(imageIR);
        break;
    case 13:
        report.tvImage = sobelOperator(imageTV);
        report.irImage = sobelOperator(imageIR);
        break;
    case 14:
        report.tvImage = laplaceOperator(imageTV);
        report.irImage = laplaceOperator(imageIR);
        break;
    case 15:
        report.tvImage = cannyEdges(imageTV);
        report.irImage = cannyEdges(imageIR);
        break;
    case 16:
        report.tvImage = extractValidInformation(imageTV, 11, 5);
        report.irImage = extractValidInformation(imageIR, 11, 2);
        break;
    case 17:
        report.tvImage = testingAnalyze(imageTV);
        report.irImage = testingAnalyze(imageIR);
        break;
    case 18:
        report.reportText = calculateIndicators(imageTV, "TV");
        report.reportText.append(calculateIndicators(imageIR, "IR"));
        break;
    }
    if(!report.irImage.empty() && !report.tvImage.empty())
    {
        report.reportText = calculateIndicators(report.tvImage, "TV processed");
        report.reportText.append(calculateIndicators(report.irImage, "IR processed"));
    }
    return report;
}

QStringList ImageAnalysis::getAnalyzeMethodList()
{
    QStringList ret;
    ret<<"Image histogram"<<"Histogram equalization"
       <<"Image negative"<<"Negative with threshold"
       <<"Negative with auto threshold"
       <<"Negative with threshold sharpened"<<"Negative substraction"
       <<"Equalization + negative"
       <<"Mean thresholding"<<"Gauss thresholding"
       <<"Negative IR thresholding"<<"High value threshold"
       <<"Gaussian substraction"<<"Sobel operator"<<"Laplace operator"
       <<"Canny"
       <<"Extract valid information"<<"Testing case"
       <<"Calculate indicators (text output)";
    return ret;
}

void ImageAnalysis::setImageTV(const Mat &value)
{
    imageTV = value;
}

void ImageAnalysis::setImageIR(const Mat &value)
{
    imageIR = value;
}

QString ImageAnalysis::getElapsedTime()
{
    return QString("Time elapsed: " + QString::number(elapsedTimer.elapsed()) + " ms");
}

Mat ImageAnalysis::alphaBlending(Mat &inputOne, Mat &inputTwo)
{
    cv::Mat dst;
    Mat input = convertToGray(inputOne);
    Mat input2 = convertToGray(inputTwo);
    addWeighted( input, 0.5, input2, 0.5, 0.0, dst);
    return dst;
}

Mat ImageAnalysis::equalizeHistogram(Mat &input)
{
    Mat output = convertToGray(input);
    equalizeHist( output, output );
    return output;
}

Mat ImageAnalysis::calculateHistogram(Mat &input)
{
    int histSize = 256;

    float range[] = { 0, 256 } ;
    const float* histRange = { range };

    bool uniform = true; bool accumulate = true;

    Mat hist;
    Mat grayInput = convertToGray(input);

    calcHist( &grayInput, 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate );

    int hist_w = 512;
    int hist_h = 500;
    int bin_w = cvRound( (double) hist_w/histSize );

    Mat histImage( hist_h, hist_w, CV_8UC1, Scalar( 0,0,0) );

    normalize(hist, hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );

    for( int i = 1; i < histSize; i++ )
    {
      line( histImage, Point( bin_w*(i-1), hist_h - cvRound(hist.at<float>(i-1)) ) ,
                       Point( bin_w*(i), hist_h - cvRound(hist.at<float>(i)) ),
                       Scalar( 255, 255, 255), 2, 8, 0  );
    }
    return histImage;
}

Mat ImageAnalysis::findContoursInImage(Mat &input)
{
       Mat gray = convertToGray(input);
       Canny(gray, gray, 100, 200, 3);

       vector<vector<Point> > contours;
       vector<Vec4i> hierarchy;
       RNG rng(12345);
       findContours( gray, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

       Mat drawing = Mat::zeros( gray.size(), CV_8UC3 );
       for( int i = 0; i< contours.size(); i++ )
       {
           Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
           drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, Point() );
       }
       return drawing;
}

Mat ImageAnalysis::calculateNegative(Mat &input)
{
    Mat ret = convertToGray(input);
    Mat subMat = Mat::ones(ret.size(), ret.type())*255;

    subtract(subMat, ret, ret);

    return ret;
}

Mat ImageAnalysis::calculateNegativeWithThreshold(Mat &input)
{
    Mat negative = calculateNegative(input);
    normalize(negative, negative, 0, 180, NORM_MINMAX);
    Mat thresholded = max(negative, convertToGray(input));
    return thresholded;
}

Mat ImageAnalysis::calculateNegativeWithAutoThreshold(Mat &inputForProc, Mat inputForHist)
{
    int histSize = 256;
    float range[] = { 0, 256 } ;
    const float* histRange = { range };
    bool uniform = true; bool accumulate = true;
    Mat hist;
    Mat grayInput = convertToGray(inputForHist);
    calcHist( &grayInput, 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate );
    int mean = 0, count;
    count = inputForProc.rows * inputForProc.cols;
    for(int i=0; i<hist.rows; i++)
    {
        mean += (int)hist.at<float>(i,0) * i;
    }
    mean /= count;
    Mat negative = calculateNegative(inputForProc);
    normalize(negative, negative, 0, mean, NORM_MINMAX);
    Mat thresholded = max(negative, convertToGray(inputForProc));
    return thresholded;
}

Mat ImageAnalysis::calculateNegativeWithThresholdSharpened(Mat &input)
{
    Mat thresholded = calculateNegativeWithThreshold(input);
    Mat sharpener;
    GaussianBlur(thresholded, sharpener, cv::Size(0, 0), 5);
    addWeighted(thresholded, 1.5, sharpener, -0.5, 0, thresholded);
    return thresholded;
}

Mat ImageAnalysis::calculateNegativeSubstraction(Mat &input)
{
    Mat proc = convertToGray(input);
    Mat negative = calculateNegative(input);

    for(int i=0; i<proc.rows; i++)
    {
        for(int j=0; j<proc.cols; j++)
        {
            int negativePixel = negative.at<uchar>(i,j);
            int procPixel = proc.at<uchar>(i,j);
            int value = negativePixel - procPixel;
            if(value > 40)
                proc.at<uchar>(i,j) = negativePixel;
        }
    }
    return proc;
}

Mat ImageAnalysis::adaptiveMeanThresholding(Mat &input, int alpha, int beta)
{
    Mat output = convertToGray(input);
    adaptiveThreshold(output, output, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, alpha, beta);
    output = openMorphologicaly(output);
    return output;
}

Mat ImageAnalysis::adaptiveGaussThresholding(Mat &input)
{
    Mat output = convertToGray(input);
    adaptiveThreshold(output, output, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 7, 1);
    output = openMorphologicaly(output);
    return output;
}

Mat ImageAnalysis::openMorphologicaly(Mat &input)
{
    Mat output;
    Mat element = getStructuringElement(MORPH_ELLIPSE, Size(3,3), Point(0,0));
    erode(input, output, element);
    dilate(output, output, element);
    return output;
}

Mat ImageAnalysis::highThreshold(Mat &input)
{
    Mat output = convertToGray(input);
    threshold(output, output, 200, 255, THRESH_BINARY);
    return output;
}

Mat ImageAnalysis::extractValidInformation(Mat &input, int alpha, int beta)
{
    Mat output = adaptiveMeanThresholding(input, alpha, beta);
    Mat element = getStructuringElement(MORPH_ELLIPSE, Size(4,4), Point(0,0));
    erode(output, output, element);
    dilate(output, output, element);
    return output;
}

Mat ImageAnalysis::gaussianSubstraction(Mat &input)
{
    Mat proc = convertToGray(input);
    GaussianBlur(proc, proc, cv::Size(0, 0), 3);
    subtract(convertToGray(input), proc, proc);
    threshold(proc, proc, 5, 255, THRESH_BINARY);
    proc = openMorphologicaly(proc);
    return proc;
}

Mat ImageAnalysis::sobelOperator(Mat &input)
{
    Mat proc = convertToGray(input);
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;

    Mat grad;
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;

    Sobel( proc, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_x, abs_grad_x );

    Sobel( proc, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_y, abs_grad_y );

    addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );
    threshold(grad, grad, 15, 255, THRESH_BINARY);
    grad = openMorphologicaly(grad);
    return grad;
}

Mat ImageAnalysis::laplaceOperator(Mat &input)
{
    Mat proc = convertToGray(input);
    Mat abs_dst;
    Mat dst;
    int kernel_size = 3;
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;
    Laplacian( proc, dst, ddepth, kernel_size, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( dst, abs_dst );
    threshold(abs_dst, abs_dst, 15, 255, THRESH_BINARY);
    abs_dst = openMorphologicaly(abs_dst);
    return abs_dst;
}

Mat ImageAnalysis::cannyEdges(Mat &input)
{
    Mat proc = convertToGray(input);
    Mat dst, cdst;
    Canny(proc, dst, 10, 180, 3);
    return dst;
}

Mat ImageAnalysis::testingAnalyze(Mat &input)
{
    Mat proc = convertToGray(input);

    for(int i=0; i<proc.rows; i++)
    {
        for(int j=0; j<proc.cols; j++)
        {
            int procPixel = proc.at<uchar>(i,j);
            int addedValue;
            if(procPixel < 40)
            {
                addedValue = 40;
            }
            else if(procPixel < 120)
            {
                addedValue = 120;
            }
            if (procPixel < 120 )
            {
                int value = procPixel + addedValue;
                proc.at<uchar>(i,j) = value;
            }
        }
    }
    return proc;
}

QString ImageAnalysis::calculateIndicators(Mat &input, QString imageType)
{
    QString ret;
    Scalar mean, stddev;
    Mat image = convertToGray(input);
    meanStdDev(image, mean, stddev);
    ret.append(imageType + "\n");
    ret.append("Mean: " + QString::number(mean.val[0]) + " ");
    ret.append("Standard deviation: " + QString::number(stddev.val[0]));
    ret.append("\n");

    double *min = new double();
    double *max = new double();
    minMaxIdx(image, min, max);
    ret.append("Min value: " + QString::number(*min) + " ");
    ret.append("Max value: " + QString::number(*max));
    ret.append("\n");
    int i = image.type();

    double cvNorm = norm(image);
    ret.append("Norm: " + QString::number(cvNorm)+ " ");
    if(!report.irImage.empty() && !report.tvImage.empty())
    {
        Mat proc = convertToGray(report.irImage);
        Mat proc2 = convertToGray(report.tvImage);
        cvNorm = norm(proc, proc2);
        ret.append("Norm absolute: " + QString::number(cvNorm)+ " ");
    }
    ret.append("\n");

    int nonZero = countNonZero(image);
    ret.append("Non zero pixels: " + QString::number(nonZero) + " ");
    Scalar cvSum = sum(image);
    ret.append("Value sum: " + QString::number(cvSum.val[0]) + "\n");
    delete min, max;
    return ret;
}

Mat ImageAnalysis::convertToGray(Mat &input)
{
    Mat output;
    if(input.channels() != 1)
        cvtColor( input, output, CV_BGR2GRAY );
    else
        return input;
    return output;
}

