#define MAINFILE

#include <iostream>
#include <QtGui>
#include <QApplication>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>

#include <include/mainwindow.h>

using namespace cv;
using namespace std;

int main( int argc, char** argv )
{
    QApplication app(argc, argv);
    MainWindow w(argc, argv);
    w.show();
    return app.exec();
}
