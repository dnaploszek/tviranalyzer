﻿#include <include/mainwindow.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <QFileDialog>
#include "ui_mainwindow.h"


//===============================================================================
//======================= GLOBAL, CONSTRUCTOR, DESTRUCTOR =======================
//===============================================================================

MainWindow::MainWindow( int argc, char** argv, QWidget *parent ) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->analyzeListWidget->insertItems(0, analyzer.getAnalyzeMethodList());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_analyzeButton_clicked()
{
    if (pathTV == "" || pathIR == "")
        return;

    AnalysisReport report = analyzer.analyze(ui->analyzeListWidget->currentRow());
    QString log = report.reportText;
    log.append(analyzer.getElapsedTime());
    ui->logBrowser->setText(log);
    if(report.tvImage.empty() || report.irImage.empty())
        return;
    cv::Mat tvImage = report.tvImage;
    cv::Mat irImage = report.irImage;
    displayImageGray(tvImage, ui->imageTVProcDisplay);
    displayImageGray(irImage, ui->imageIRProcDisplay);
}

void MainWindow::displayImageRGB(Mat &mat, QLabel *label)
{
    Size size = getResizeSize(Size(mat.cols, mat.rows), Size(label->width(), label->height()));
    cv::resize(mat, mat, Size(size.width - 10, size.height - 10), 0, 0, INTER_LINEAR);
    QImage imdisplay((uchar*)mat.data, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
    label->setPixmap(QPixmap::fromImage(imdisplay));
}

void MainWindow::displayImageGray(Mat &mat, QLabel *label)
{
    Size size = getResizeSize(Size(mat.cols, mat.rows), Size(label->width(), label->height()));
    cv::resize(mat, mat, Size(size.width - 10, size.height - 10), 0, 0, INTER_LINEAR);
    QImage imdisplay((uchar*)mat.data, mat.cols, mat.rows, mat.step, QImage::Format_Grayscale8);
    label->setPixmap(QPixmap::fromImage(imdisplay));
}

Size MainWindow::getResizeSize(Size imageSize, Size frameSize)
{
    if (imageSize.width < frameSize.width && imageSize.height < frameSize.height)
        return imageSize;

    float wRatio = ((float)imageSize.width / (float)frameSize.width);
    float hRatio = ((float)imageSize.height / (float)frameSize.height);
    float ratio = max(wRatio, hRatio);

    return Size((int)(imageSize.width / ratio), (int)(imageSize.height / ratio));
}

void MainWindow::on_openTVImageButton_clicked()
{
    pathTV = QFileDialog::getOpenFileName(
                    0, "Wybierz obraz TV ",
                    "F:/Workspace/DaneTestowe", tr("Image Files (*.png *.jpg *.bmp)") );
    if(pathTV == "")
        return;

    cv::Mat imageTV = cv::imread(pathTV.toStdString(), 1);
    cv::resize(imageTV, imageTV, Size(500, 500), 0, 0, INTER_LINEAR);
    analyzer.setImageTV(imageTV);
    displayImageRGB(imageTV, ui->imageTVDisplay);
}

void MainWindow::on_openIRImageButton_clicked()
{
    pathIR = QFileDialog::getOpenFileName(
                     0, "Wybierz obraz IR ",
                     "F:/Workspace/DaneTestowe", tr("Image Files (*.png *.jpg *.bmp)")  );
    if(pathIR == "")
        return;

    cv::Mat imageIR = cv::imread(pathIR.toStdString(), 1);
    cv::resize(imageIR, imageIR, Size(500, 500), 0, 0, INTER_LINEAR);
    analyzer.setImageIR(imageIR);
    displayImageRGB(imageIR, ui->imageIRDisplay);
}
